/*
 * factory.hpp
 *
 *  Created on: 28/09/2014
 *      Author: root
 */
#ifndef FACTORY_HPP_
#define FACTORY_HPP_

#include <string>
#include <fstream>
#include "../Triangulacao/triangulacao_basica.hpp"
#include "../AlphaShapes/alpha.hpp"
#include "../Skin/skin.hpp"
#include "../powercrust/hullmain.hpp"
#include "../conversores/off2obj.hpp"

#define RESOURCES "resources/"
#define SAIDAPC "resources/saida/PowerCrust/"
#define SAIDATRI "resources/saida/Triangulação/"
#define SAIDAALPHA "resources/saida/AlphaShapes/"
#define SAIDASKIN "resources/saida/Skin/"
#define EXECUTANDO "resources/executando/"
#define ENTRADA "resources/entrada/"
#define TAILVIEW "resources/protected/view.tail"
#define HEADVIEW "resources/protected/view.head"

/**
 * Executa  algoritmo de triangulação Básico
 *
 * @param inputFile arquivo de entrada da nuvem de pontos (xyz, pts, off)
 * @return int (1 sucesso / 0 falha)
 */
int executeTringulation(std::string inputFile);

/**
 * Executa  algoritmo de Skin
 *
 * @param inputFile arquivo de entrada da nuvem de pontos (xyz, pts, off)
 * @return int (1 sucesso / 0 falha)
 */
int executeSkin(std::string inputFile, double shrinkfactor = 0.01);

/**
 * Executa  algoritmo Alpha Shapes
 *
 * @param inputFile arquivo de entrada da nuvem de pontos (xyz, pts, off)
 * @param alldatapoints conforme doc do CGAL (Default false)
 * @param alphaValue valor aplha -1 para encontrar o valor optimo (DEfault)
 * @return int (1 sucesso / 0 falha)
 *
 *  Doc do CGAL:
 * find the minimum alpha that satisfies the properties
 *
 * false converte->
 * (1) nb_components solid components <= nb_components
 *
 * true converte ->
 * (2) all data points on the boundary or in its interior
 *
 */

int executeAlphaShapes(std::string inputFile, bool alldatapoints = false,
		int alphaValue = -1);

/**
 * Executa  algoritmo Power Crust
 *
 * @param inputFile arquivo de entrada da nuvem de pontos (xyz, pts, off)
 * @param bBadPoles - propaga os polos ruins 0 || 1,
 * @param vlTheta  angulo do deep intersection | valor de coseno minimo  para o angulo  dihedral entre as "polar balls". Defaul 0.0
 * @param vlDeep  mesmo que o theta  para a marcação dos polos para a segunda iteração com os polos não marcados. Default 0.0
 * @param vlMult_up fator de multiplicação aplicado antes do arredondamento Default 100000;
 * @param vlEst_R valor estimado de r, para eliminar os polos da segunda iteração | bom 0.6 ou 1
 * @param vlSeed  seed para o srand(seed); Default 0
 * @param poleinoput -> retirado da implementação, sempre gerar os polos
 * @return int (1 sucesso / 0 falha)
 */
int executePowerCrust(std::string inputfile, int bBadPoles = 0, double vlEst_R =
		1, long vlSeed = 0, double vlMult_up = 100000, double vlTheta = 0.0,
		double vlDeep = 0.0);

/**
 * Função que recebe o caminho para o arquivo de entrada da nuvem de pontos.
 * Os formatos suportados são .OFF , .pts e .xyz
 *
 *@param filename  - path do arquivo de entrada da nuvem de pontos
 *
 */
std::string genPtsFromInputFile(std::string filename);

/**
 * Função que recebe duas strings e testa a igualdade
 * Não é case sensitive
 *
 *@param a string comparada
 *@param b string comparada
 */
bool extCompare(const std::string& a, const std::string& b);

#endif /* FACTORY_HPP_ */
