$(function () {
    var file;
    // listener da seleção do arquivo.
    $('#fileselect').bind("change", function (e) {
        var files = e.target.files || e.dataTransfer.files;
        // variavel handle do arquivo
        file = files[0];
    });

    // funçao click
    $('#uploadbutton').click(function () {
        var serverUrl = '/input/' + file.name;

        $.ajax({
            type: "POST",
            beforeSend: function (request) {
                request.setRequestHeader("Content-Type", file.type);
            },
            url: serverUrl,
            data: file,
            processData: false,
            contentType: false,
            success: function (data) {

                var obj = jQuery.parseJSON(data);
                if (obj.error) {
                    alert(obj.error);
                } else {
                    alert(obj.data);
                }
            },
            error: function (data) {
                var obj = jQuery.parseJSON(data);
                alert(obj.error);
            }
        });
    });
});


//function loadList() {
//    $.getJSON('/list', function (jd) {
//        files = jd;
//        //    console.log("inicio for " + files);
//    });
//}

var formularios = {
    "Algoritmos": [
                "teste"
            ]
};
//

var list = document.getElementById('list');

var viewer = document.getElementById('viewer');
if (/(iPad|iPhone|iPod)/g.test(navigator.userAgent)) {

    viewer.addEventListener('load', function (event) {

        viewer.contentWindow.innerWidth -= 10;
        viewer.contentWindow.innerHeight -= 2;
    });
}

var container = document.createElement('div');
list.appendChild(container);
//var button = document.createElement('div');
//button.id = 'button';
//button.textContent = 'teste post';
//button.addEventListener('click', function (event) {
//    var http = new XMLHttpRequest();
//    var Xurl = "http://localhost:1111/hello";
//    var Xparams = "data=teste";
//    http.open("POST", Xurl + "?" + Xparams, true);
//    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//    http.setRequestHeader("Content-length", Xparams.length);
//    http.setRequestHeader("Connection", "close");
//    http.onreadystatechange = function () {//Call a function when the state changes.
//        if (http.readyState == 4 && http.status == 200) {
//            alert(http.responseText);
//        } else {
//            alert("Não foi possível comunicar com a biblioteca!")
//        }
//    }
//    http.send(null);
//    //var array = location.href.split( '/' );
//    //array.pop();
//
//    //window.open( 'view-source:' + array.join( '/' ) + '/' + selected + '.html' );
//
//}, false);
//button.style.display = 'none';
//document.body.appendChild(button);
var divs = {};
var selected = null;


// entrada
var entrada;
loadEntrada();
// executando
var executando;
loadExecutando();
// saida
var saida;
loadSaida();

for (var key in entrada) {

    var section = entrada[key];
    var div = document.createElement('h2');
    //div.textContent = key;
    div.textContent = "Arquivos de Entrada:";
    container.appendChild(div);
    for (var i = 0; i < section.length; i++) {
        (function (file) {
            var name = section[i];
            var div = document.createElement('div');
            div.className = 'link';
            div.textContent = name;
            div.addEventListener('click', function (e) {
                downloadEntrada(file, e);
            });
            container.appendChild(div);
            divs[file] = div;
        })(section[i]);
    }
}




for (var key in executando) {
    var section = executando[key];
    var div = document.createElement('h2');
    //div.textContent = key;
    div.textContent = "Executando:";
    container.appendChild(div);
    for (var i = 0; i < section.length; i++) {
        (function (file) {
            var name = section[i];
            var div = document.createElement('div');
            //            div.className = 'link';
            div.textContent = name;
            //            div.addEventListener('click', function () {
            //                loadEntrada(file);
            //});
            container.appendChild(div);
            divs[file] = div;
        })(section[i]);
    }
}


var div = document.createElement('h2');
div.textContent = "Saidas:";
container.appendChild(div);


for (var key in saida) {

    var div = document.createElement('h3');

    // Tratamento da nomeclatura
    if (key == 'PowerCrust') {
        div.textContent = key + ':';
    } else if (key == 'AlphaShapes') {
        div.textContent = key + ':';
    } else if (key == 'Triangulcao') {
        div.textContent = 'Triangulação:';
    } else if (key == 'Skin') {
        div.textContent = key + ':';
    } else {
        div.textContent = key;
    }
    container.appendChild(div);
    var section = saida[key];

    //var ul = document.createElement('ul');
    for (var i = 0; i < section.length; i++) {
        (function (file) {
            var name = section[i];
            var div = document.createElement('div');
            div.className = 'link';

            div.textContent = name;
            div.addEventListener('click', function () {
                loadPC(file);
            });
            container.appendChild(div);
            divs[file] = div;
        })(section[i]);
    }
    //container.appendChild(ul);

}


var downloadEntrada = function (file, e) {

    //    var serverUrl = '/Input/'+file;
    //    $.ajax({
    //        type: "GET",
    //        url: serverUrl,
    //        processData: false,
    //        contentType: false,
    //        async: false,
    //        success: function (data) {
    //            entrada = jQuery.parseJSON(data);
    //        },
    //        error: function (data) {
    //            var obj = jQuery.parseJSON(data);
    //            alert(obj.error);
    //        }
    //    });
    e.preventDefault(); //stop the browser from following
    window.location.href = '/input/' + file;






    //loading da view
    //    if (selected !== null)
    //        divs[selected].className = 'link';
    //    divs[file].className = 'link selected';
    //    window.location.hash = file;
    //    viewer.src = '/input/' + file;
    //    viewer.focus();
    //    //button.style.display = '';
    //    selected = file;
    //    alert(selected);
};

//if (window.location.hash !== '') {
//
//    load(window.location.hash.substring(1));
//}


var loadPC = function (file) {
    //loading da view
    if (selected !== null)
        divs[selected].className = 'link';
    divs[file].className = 'link selected';
    window.location.hash = file;
    viewer.src = '/PowerCrust/' + file+'.html';
    viewer.focus();
    //button.style.display = '';
    selected = file;
   // alert(selected);
}



function loadEntrada() {
    var serverUrl = '/Input';
    $.ajax({
        type: "GET",
        url: serverUrl,
        processData: false,
        contentType: false,
        async: false,

        success: function (data) {
            entrada = jQuery.parseJSON(data);
        },
        error: function (data) {
            var obj = jQuery.parseJSON(data);
            alert(obj.error);
        }
    });
};


function loadSaida() {
    var serverUrl = '/Output';
    $.ajax({
        type: "GET",
        url: serverUrl,
        processData: false,
        contentType: false,
        async: false,

        success: function (data) {
            saida = jQuery.parseJSON(data);
        },
        error: function (data) {
            var obj = jQuery.parseJSON(data);
            alert(obj.error);
        }
    });
};

function loadExecutando() {
    var serverUrl = '/Exec';
    $.ajax({
        type: "GET",
        url: serverUrl,
        processData: false,
        contentType: false,
        async: false,

        success: function (data) {
            executando = jQuery.parseJSON(data);
        },
        error: function (data) {
            var obj = jQuery.parseJSON(data);
            alert(obj.error);
        }
    });
};