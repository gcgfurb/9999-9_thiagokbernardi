/*
 This file is part of libhttpserver
 Copyright (C) 2011, 2012, 2013, 2014 Sebastiano Merlino

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 USA
 */

#include "httpserver.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "factory/factory.hpp"
#include "dirent.h"
#include "sys/stat.h"

using namespace httpserver;
using namespace rapidjson;

bool block = false;

class Input: public http_resource<Input> {
public:
	void render(const http_request&, http_response**);
	void render_POST(const http_request&, http_response**);
	void extracDirs(Writer<StringBuffer>& writer, const char* dire);
	void set_some_data(const std::string &s) {
		data = s;
	}
	std::string data;
}
;

void Input::extracDirs(Writer<StringBuffer>& writer, const char* dire) {

	DIR *diretorio;
	struct dirent *entrada;
	diretorio = opendir(dire);

	if (diretorio != NULL) {

		writer.StartArray();

		while ((entrada = readdir(diretorio))) {

			// verifica se é marcador de diretorio
			if (!extCompare(entrada->d_name, ".")
					&& !extCompare(entrada->d_name, "..")) {
				//std::cout << entrada->d_name << std::endl;
				writer.String(entrada->d_name);
			}
		}

		writer.EndArray();

	} else {
		writer.Null();
	}
}

void Input::render(const http_request& req, http_response** res) {

	// inicializa json de resposta
	StringBuffer json;

	// verifica se o nome do arquilvo foi passado no path
	if (req.get_path_pieces_size() > 1) {

		// se foi é proque foi solicitado o download

		std::stringstream s;

		std::cout << "Download |" << ENTRADA << req.get_path_piece(1)
				<< std::endl;

		std::ifstream ifs((ENTRADA + req.get_path_piece(1)).c_str());

		if (ifs) {
			s << ifs.rdbuf();
			ifs.close();

			*res =
					new http_response(
							http_response_builder(s.str(), 200, "text/pts").string_response());

		} else {
			*res = new http_response(
					http_response_builder(
							"{\"error\":\"Arquivo não encontrado\"}", 403,
							"text/plain").string_response());

		}

		return;
	}

	// inicializa o writer
	Writer<StringBuffer> writer(json);

	writer.StartObject();
	//-->
	// escreve os arquivos de entrada
	writer.String("Input");
	extracDirs(writer, ENTRADA);
	//-->-->
	//-->
	writer.EndObject();

	*res =
			new http_response(
					http_response_builder(json.GetString(), 200, "text/plain").string_response());

	return;
}
void Input::render_POST(const http_request& req, http_response** res) {

	// inicializa json de resposta
	StringBuffer json;
	// inicializa o writer
	Writer<StringBuffer> writer(json);

	std::string conteudo;
	std::string filename;

	// atribui conteudo
	req.get_content(conteudo);

	// verifica se o nome do arquilvo foi passado no path
	if (req.get_path_pieces_size() <= 1) {

		std::cout << "velifica o nome do arquivo " << std::endl;

		writer.StartObject();
		writer.String("data");
		writer.String(" ");
		writer.String("error");
		writer.String("Nome do arquivo não informado! (Requisição inválida)");
		writer.EndObject();

		std::cout << json.GetString() << std::endl;

		*res = new http_response(http_response_builder(json.GetString(), 200, // Requisição inválida,
				"text/plain").string_response());
		return;

	}

	//std::cout << "path index 1 " << req.get_path_piece(1) << std::endl;

	// cria ofstream de gravação

	block = true;
	filename = ENTRADA + req.get_path_piece(1);

	std::ofstream of(filename.c_str());

	// verifica se foi possivel criar o arquivo

	if (of.is_open()) {

		of << conteudo;
		of.close();
		block = false;

	} else {

		writer.StartObject();
		writer.String("data");
		writer.String(" ");
		writer.String("error");
		writer.String(
				"Não foi possivel criar o arquivo! Verifique as permissãoes de pasta! (Não autorizado)");
		writer.EndObject();

		block = false;

		*res = new http_response(http_response_builder(json.GetString(), 200, // Não autorizado,
				"text/plain").string_response());
		return;
	}

	//monta retorno json
	std::string data = "Arquivo recebido com sucesso: ";
	data += req.get_path_piece(1);

	writer.StartObject();
	writer.String("data");
	writer.String(data.c_str()); // escrever o local de saida do objeto
	writer.String("error");
	writer.Null();
	writer.EndObject();

	*res =
			new http_response(
					http_response_builder(json.GetString(), 200, "text/plain").string_response());

	return;
}

class Output: public http_resource<Output> {
public:
	void render(const http_request&, http_response**);
	void extracDirs(Writer<StringBuffer>& writer, const char* dire);
	void set_some_data(const std::string &s) {
		data = s;
	}
	std::string data;
};

void Output::extracDirs(Writer<StringBuffer>& writer, const char* dire) {

	DIR *diretorio;
	struct dirent *entrada;
	diretorio = opendir(dire);

	if (diretorio != NULL) {

		writer.StartArray();

		while ((entrada = readdir(diretorio))) {

			// verifica se é marcador de diretorio
			if (!extCompare(entrada->d_name, ".")
					&& !extCompare(entrada->d_name, "..")) {
				//std::cout << entrada->d_name << std::endl;
				writer.String(entrada->d_name);
			}
		}

		writer.EndArray();

	} else {
		writer.Null();
	}
}
void Output::render(const http_request& req, http_response** res) {

	// inicializa json de resposta
	StringBuffer json;
	// inicializa o writer
	Writer<StringBuffer> writer(json);

	writer.StartObject();
	//-->
	//writer.String("Arquivos gerados:");
	//writer.StartArray();
	//-->-->

	writer.String("PowerCrust");
	extracDirs(writer, SAIDAPC);
	writer.String("AlphaShapes");
	extracDirs(writer, SAIDAALPHA);
	writer.String("Triangulcao");
	extracDirs(writer, SAIDATRI);
	writer.String("Skin");
	extracDirs(writer, SAIDASKIN);

	//writer.EndArray();
	//-->-->
	//-->
	writer.EndObject();

	*res =
			new http_response(
					http_response_builder(json.GetString(), 200, "text/plain").string_response());

	return;
}

class Exec: public http_resource<Exec> {
public:
	void render(const http_request&, http_response**);
	void extracDirs(Writer<StringBuffer>& writer, const char* dire);
	void set_some_data(const std::string &s) {
		data = s;
	}
	std::string data;
};

void Exec::extracDirs(Writer<StringBuffer>& writer, const char* dire) {

	DIR *diretorio;
	struct dirent *entrada;
	diretorio = opendir(dire);

	if (diretorio != NULL) {

		writer.StartArray();

		while ((entrada = readdir(diretorio))) {

			// verifica se é marcador de diretorio
			if (!extCompare(entrada->d_name, ".")
					&& !extCompare(entrada->d_name, "..")) {
				//std::cout << entrada->d_name << std::endl;
				writer.String(entrada->d_name);
			}
		}

		writer.EndArray();

	} else {
		writer.Null();
	}
}

void Exec::render(const http_request& req, http_response** res) {

	// inicializa json de resposta
	StringBuffer json;
	// inicializa o writer
	Writer<StringBuffer> writer(json);

	writer.StartObject();
	//-->
	writer.String("Exec");
	extracDirs(writer, EXECUTANDO);
	//-->-->
	//-->
	writer.EndObject();

	*res =
			new http_response(
					http_response_builder(json.GetString(), 200, "text/plain").string_response());

	return;
}

class PowerCrust: public http_resource<PowerCrust> {
public:
	void render_GET(const http_request&, http_response**);
	void set_some_data(const std::string &s) {
		data = s;
	}
	std::string data;
};

void PowerCrust::render_GET(const http_request& req, http_response** res) {

	// verifica se o nome do arquilvo foi passado no path
	if (req.get_path_pieces_size() == 2) {

		//Verifica se foi solicitado html da view
		std::string file = req.get_path_piece(1);

		std::string ext = file.substr(file.length() - 5, 5);

		std::cout << ext << std::endl;

		if (extCompare(".html", ext)) {
			// monta view
			// retira extençao
			std::string fileview = file.substr(0, file.length() - 5);
			std::stringstream view;

			std::cout << "Download PowerCrust|" << SAIDAPC << file << std::endl;

			std::ifstream ifs((HEADVIEW));

			if (ifs) {
				view << ifs.rdbuf();
				ifs.close();

			} else {
				*res =
						new http_response(
								http_response_builder(
										"{\"error\":\"Não foi possivel montar a VIEW - HEAD Arquivo não encontrado\"}",
										403, "text/plain").string_response());

				return;
			}

			ifs.open((SAIDAPC + fileview).c_str(), std::ifstream::in);
			if (ifs) {

				view <<  fileview;
				ifs.close();

			} else {
				*res = new http_response(
						http_response_builder(
								"{\"error\":\"Não foi possivel montar a VIEW - "
										+ fileview + " não encontrado\"}", 403,
								"text/plain").string_response());

				return;

			}

			ifs.open((TAILVIEW), std::ifstream::in);

			if (ifs) {
				view << ifs.rdbuf();
				ifs.close();

			} else {
				*res =
						new http_response(
								http_response_builder(
										"{\"error\":\"Não foi possivel montar a VIEW - TAIL Arquivo não encontrado\"}",
										403, "text/plain").string_response());

				return;
			}

			*res =
					new http_response(
							http_response_builder(view.str(), 200, "text/html").string_response());

			return;
		}

		//

		// se foi é proque foi solicitado o download
		std::stringstream s;

		std::cout << "Download PowerCrust|" << SAIDAPC << file << std::endl;

		std::ifstream ifs((SAIDAPC + file).c_str());

		if (ifs) {
			s << ifs.rdbuf();
			ifs.close();

			*res = new http_response(
					http_response_builder(s.str(), 200,
							"application/wavefront-obj").string_response());

		} else {
			*res = new http_response(
					http_response_builder(
							"{\"error\":\"Arquivo não encontrado\"}", 403,
							"text/plain").string_response());

			return;
		}

	}
//TODO:erro
// inicializa json de resposta
	StringBuffer json;
//	// inicializa o writer
//	Writer<StringBuffer> writer(json);
//
//	writer.StartObject();
//	//-->
//	// escreve os arquivos de entrada
//	writer.String("Input");
//	extracDirs(writer, ENTRADA);
//	//-->-->
//	//-->
//	writer.EndObject();
//
//	*res =
//			new http_response(
//					http_response_builder(json.GetString(), 200, "text/plain").string_response());
//
//	return;
}

//class uploadcloud: public http_resource<uploadcloud> {
//public:
//	void render(const http_request&, http_response**);
//
//	void set_some_data(const std::string &s) {
//		data = s;
//	}
//	std::string data;
//};

class Index: public http_resource<Index> {
public:
	void render(const http_request&, http_response**);

};

void Index::render(const http_request& req, http_response** res) {

	std::stringstream s;
	std::string file = req.get_path();
	std::string filetype = "text/html";

	std::cout << "chamada |" << file << "|" << std::endl;

// se nada for passado retorna a view
	if (file.length() < 2 || file.empty()) {
		file = "/index.html";
	}
	std::string ext = file.substr(file.length() - 3, 3);

	if (extCompare(ext, ".js")) {
		filetype = "text/javascript";
	} else if (extCompare(ext, "css")) {
		filetype = "text/css";
	} else if (extCompare(ext, "png")) {
		filetype = "image/png";
	} else if (extCompare(ext, "ico")) {
		filetype = "image/ico";
	}

	std::ifstream ifs(("resources/view" + file).c_str());

	if (ifs) {
		s << ifs.rdbuf();
		ifs.close();

		*res =
				new http_response(
						http_response_builder(s.str(), 200, filetype).string_response());

	} else {
		*res = new http_response(
				http_response_builder("{\"error\":\"Arquivo não encontrado\"}",
						403, "text/plain").string_response());

	}

}

void createDirs() {

	mkdir(RESOURCES, 755);
	mkdir(ENTRADA, 755);
	mkdir(EXECUTANDO, 755);
	mkdir(SAIDAALPHA, 755);
	mkdir(SAIDAPC, 755);
	mkdir(SAIDATRI, 755);
	mkdir(SAIDASKIN, 755);
}

int main() {

	createDirs();
	webserver ws = create_webserver(80).max_threads(5);
//uploadcloud ucr;
	Index twr;
	Input inputr;
	Output outputr;
	Exec execr;
	PowerCrust pcr;

//ws.register_resource("/uploadCloud", &ucr, true);
	ws.register_resource("/Input", &inputr, true);
	ws.register_resource("/Output", &outputr, true);
	ws.register_resource("/Exec", &execr, true);
	ws.register_resource("/PowerCrust", &pcr, true);
	ws.register_resource("/", &twr, true);

	ws.start(true);
	return 0;
}
