/*
 * hullmain.hpp
 *
 *  Created on: 02/10/2014
 *      Author: root
 */

#ifndef HULLMAIN_HPP_
#define HULLMAIN_HPP_

#include <float.h>
#include <math.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <string.h>
#include <ctype.h>
#include <string>
#define POINTSITES 1

#include "hull.h"

std::string powerCrust(std::string inputfile, int bBadPoles = 0,
		double vlEst_R = 1, long vlSeed = 0, double vlMult_up = 100000,
		double vlTheta = 0.0, double vlDeep = 0.0);

#endif /* HULLMAIN_HPP_ */
