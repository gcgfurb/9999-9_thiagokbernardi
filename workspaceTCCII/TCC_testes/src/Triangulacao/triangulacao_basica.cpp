/*
 * triangulacao_basica.cpp
 *
 *  Created on: 25/08/2014
 *      Author: root
 */

#include "triangulacao_basica.hpp"

std::string Triangulacao(std::string file) {

	//Ativa o visualizador em tempo real Geom
	//CGAL::Geomview_stream gv(CGAL::Bbox_3(-1, -1, -1, 5, 5, 5));
	//gv.set_line_width(1);

	//gv.set_trace(true);
	//gv.set_bg_color(CGAL::Color(0, 200, 200));
	//gv.clear();

	//gv.set_wired(false);
	//gv << CGAL::RED;

	// Constroi a lista de pontos

	std::list<Point> L;
	std::ifstream is(file.c_str());
	Point P;
	for (; is >> P;) {
		L.push_front(P);
	}
	is.close();
	Triangulation T(L.begin(), L.end());
	Triangulation::size_type n = T.number_of_vertices();

	//gv << T;

	// reading file output;
	//	iFileT >> T1;
	//	assert(T1.is_valid());
	//	assert(T1.number_of_vertices() == T.number_of_vertices());
	//	assert(T1.number_of_cells() == T.number_of_cells());
	//	std::cout << "pressione para terminar" << std::endl;
	//	char ch;
	//	std::cin >> ch;

	std::string fileoutput = (file + ".off");
	std::ofstream of(fileoutput.c_str());

	// usa recurso da CGAL para escrita do arquivo off
	//out << T;
	//out.close();
	//return fileoutput;

	// força o  ascII
	//CGAL::set_ascii_mode(of);

	// escreve o cabecalho
	//of << "OFF" << std::endl;
	// num de vertice , numero de faces sem edges
	//of << T.number_of_vertices() << ' ' << T.number_of_finite_facets() << " 0"
//				<< std::endl;
//	// Copia os pontos para o arquivo
//	std::copy(T.points_begin(), T.points_end(),
//			std::ostream_iterator<Point>(of, "\n"));

	std::vector<Triangulation::Facet> facets;

	facets.insert(facets.begin(), T.finite_facets_begin(),
			T.finite_facets_end());

	// gera as faces para o arquivo

	std::stringstream pts;
	std::stringstream ind;

	std::size_t nbf = T.number_of_finite_facets();

	for (std::size_t i = 0; i < nbf; ++i) {

		int indices[3] = { (facets[i].second + 1) % 4, (facets[i].second + 2)
				% 4, (facets[i].second + 3) % 4, };

		///necessario para  a orientação
		if (facets[i].second % 2 == 0)
			std::swap(indices[0], indices[1]);

		pts << facets[i].first->vertex(indices[0])->point() << "\n"
				<< facets[i].first->vertex(indices[1])->point() << "\n"
				<< facets[i].first->vertex(indices[2])->point() << "\n";

		ind << "3 " << 3 * i << " " << 3 * i + 1 << " " << 3 * i + 2 << "\n";
	}

	of << "OFF \n" << 3 * nbf << " " << nbf << " 0\n";
	of << pts.str();
	of << ind.str();

//
	return fileoutput;
}

