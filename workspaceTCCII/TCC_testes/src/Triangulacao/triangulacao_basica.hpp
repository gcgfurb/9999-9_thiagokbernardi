/*
 * triangulacao_basica.hpp
 *
 *  Created on: 25/08/2014
 *      Author: root
 */
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/IO/Triangulation_geomview_ostream_3.h>
#include <CGAL/Triangulation_3.h>
#include <iostream>
#include <fstream>
#include <cassert>
#include <list>
#include <vector>
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_3<K> Triangulation;
typedef Triangulation::Cell_handle Cell_handle;
typedef Triangulation::Vertex_handle Vertex_handle;
typedef Triangulation::Locate_type Locate_type;
typedef Triangulation::Point Point;
typedef Triangulation::Finite_facets_iterator facetsT_iterator;
typedef Triangulation::Facet Facet;
typedef Triangulation::Triangle Triangle;


std::string Triangulacao(std::string file);

