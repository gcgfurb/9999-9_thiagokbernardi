#include "factory.hpp"

//int execute(std::string file, int alg) {
//
//	switch (alg) {
//
//	case TRIANGULACAO:
//		break;
//	case ALPHASAHPES:
//
//		break;
//	case SKIN:
//
//		break;
//	case POWERCRUST:
//
//		break;
//
//	default:
//
//		return 1;
//		break;
//	}
//
//	return 0;
//}

/**
 * Executa  algoritmo de triangulação Básico
 *
 * @param inputFile arquivo de entrada da nuvem de pontos (xyz, pts, off)
 * @return int (1 sucesso / 0 falha)
 */
int executeTringulation(std::string inputFile) {

	try {
		std::string filename = genPtsFromInputFile(inputFile);
		std::string fileoutput = Triangulacao(filename);
		off2obj(fileoutput);

	} catch (char* error) {

		std::cout << error << std::endl;
		return 0;

	}

	return 1;
}

/**
 * Executa  algoritmo de Skin
 *
 * @param inputFile arquivo de entrada da nuvem de pontos (xyz, pts, off)
 * @return int (1 sucesso / 0 falha)
 */
int executeSkin(std::string inputFile, double shrinkfactor) {
	try {
		std::string filename = genPtsFromInputFile(inputFile);
		std::string fileoutput =  Skin(filename, shrinkfactor);
		off2obj(fileoutput);

	} catch (char* error) {

		std::cout << error << std::endl;
		return 0;

	}

	return 1;

}

/**
 * Executa  algoritmo Alpha Shapes
 *
 * @param inputFile arquivo de entrada da nuvem de pontos (xyz, pts, off)
 *@param alldatapoints Conforme doc do cgal
 *
 * Doc do CGAL:
 * find the minimum alpha that satisfies the properties
 *
 * false converte->
 * (1) nb_components solid components <= nb_components
 *
 * true converte ->
 * (2) all data points on the boundary or in its interior
 *
 *
 * @return int (1 sucesso / 0 falha)
 */
int executeAlphaShapes(std::string inputFile, bool alldatapoints,
		int alphaValue) {
	try {

		std::string filename = genPtsFromInputFile(inputFile);
		std::string fileoutput = alphaShapes(filename, alldatapoints);
		off2obj(fileoutput);

	} catch (char* error) {

		std::cout << error << std::endl;
		return 0;

	}

	return 1;
}

/**
 * Executa  algoritmo Power Crust
 *
 * @param inputFile arquivo de entrada da nuvem de pontos (xyz, pts, off)
 * @param bBadPoles - propaga os polos ruins 0 || 1,
 * @param vlTheta  angulo do deep intersection | valor de coseno minimo  para o angulo  dihedral entre as "polar balls". Defaul 0.0
 * @param vlDeep  mesmo que o theta  para a marcação dos polos para a segunda iteração com os polos não marcados. Default 0.0
 * @param vlMult_up fator de multiplicação aplicado antes do arredondamento Default 100000;
 * @param vlEst_R valor estimado de r, para eliminar os polos da segunda iteração | bom 0.6 ou 1
 * @param vlSeed  seed para o srand(seed); Default 0
 * @param poleinoput -> retirado da implementação, sempre gerar os polos
 * @return int (1 sucesso / 0 falha)
 */
int executePowerCrust(std::string inputFile, int bBadPoles, double vlEst_R,
		long vlSeed, double vlMult_up, double vlTheta, double vlDeep) {
	try {
		std::string filename = genPtsFromInputFile(inputFile);
		std::string fileoutput =  powerCrust(filename);
		off2obj(fileoutput);

	} catch (char* error) {

		std::cout << error << std::endl;
		return 0;

	}

	return 1;
}

/**
 * Função que recebe o caminho para o arquivo de entrada da nuvem de pontos.
 * Os formatos suportados são .OFF , .pts e .xyz
 *
 *@param filename  - path do arquivo de entrada da nuvem de pontos
 *
 */
std::string genPtsFromInputFile(std::string filename) {

	//char ifile[256] = "";

	std::string ext = filename.substr(filename.length() - 3, 3);

	if (extCompare(ext, "off")) {

		// gera o arquivo .pts do .off
		std::ifstream in(filename.c_str());

		std::string filenamePts = (filename + ".pts");

		std::ofstream out(filenamePts.c_str());

		std::string line;

		int nv = 0, nf = 0, ne = 0;

		// dispensa a linha de cabecalho "OFF"
		in >> line;

		// Atribui os quatificadores num de vertices nv , numero de faces nf , numero de edges
		in >> nv >> nf >> ne;

		// para cada vertice atribui x y x
		for (int i = 0; i < nv; i++) {

			double x, y, z;

			in >> x >> y >> z;
			out << x << " " << y << " " << z << "\n";

		}

		in.close();
		out.close();

		return filenamePts;

		//strcpy(ifile, (filename + ".pts").c_str());

	} else if (extCompare(ext, "xyz")) {

		// gera arquivo .pts do .xyz

		std::ifstream in(filename.c_str());

		std::string filenamePts = (filename + ".pts");

		std::ofstream out(filenamePts.c_str());

		while (!in.eof()) {

			// o formato xyz possui em cada linha a ccoordenada euclidiana 'x' 'y' 'z', seguida pela cor  'r' 'g' 'b' (x y x r g b)
			double x, y, z, nx, ny, nz;
			in >> x >> y >> z >> nx >> ny >> nz;
			// dispensamos o 'r' 'g' 'b' e passamos 'x' 'y' 'z'
			out << x << " " << y << " " << z << "\n";
		}

		in.close();
		out.close();

		return filenamePts;

		//strcpy(ifile, (filename + ".pts").c_str());
	} else if (extCompare(ext, "pts")) { // se for pts esta tudo certo 'be happy'

		return filename;

	} else {
		// se a extenção for diferente dos caso tratados gera exept
		throw "Arquivo nãom suportado: " + ext;

	}
}
/**
 * Função que recebe duas strings e testa a igualdade
 * Não é case sensitive
 *
 *@param a string comparada
 *@param b string comparada
 */
bool extCompare(const std::string& a, const std::string& b) {
	unsigned int sz = a.size();
	if (b.size() != sz)
		return false;
	for (unsigned int i = 0; i < sz; ++i)
		if (tolower(a[i]) != tolower(b[i]))
			return false;
	return true;
}

