#include "alpha.hpp"
/**
 * Executa o algoritmo AlphaShapes para a entrada
 *
 *@param inputFile Arquivo de entrada da nuvem de pontos
 *@param alldatapoints Conforme doc do cgal
 *@param alphaValue valor aplha -1 para encontrar o valor optimo (DEfault)
 * Doc do CGAL:
 * find the minimum alpha that satisfies the properties
 *
 * false converte->
 * (1) nb_components solid components <= nb_components
 *
 * true converte ->
 * (2) all data points on the boundary or in its interior
 *
 */

std::string alphaShapes(std::string inputFile, bool alldatapoints,
		int alphaValue) {

	Delaunay dt;
	std::ifstream is(inputFile.c_str());
	std::string fileoutput = (inputFile + ".off");
	//o arquivo precisa do numero de linhas(resolvido)

	int n;
	Point p;
	for (; is >> p; n++) {
		dt.insert(p);
	}
//	std::cout << n << " pontos " << std::endl;
//	std::cout << "Delaunay foi " << std::endl;

// alpha shape

	Alpha_shape_3 as(dt);
	//std::cout << "aplpha shape regular por Default"
	//<< std::endl;

	//encontra o valor ideal
	Alpha_shape_3::NT alpha_solid = as.find_alpha_solid();

	// (1) nb_components solid components <= nb_components
	//
	//true converte ->
	//(2) all data points on the boundary or in its interior
	//

	int paramAlpha = alldatapoints ? 2 : 1;

	if (alphaValue == -1) {

		Alpha_iterator opt = as.find_optimal_alpha(paramAlpha);

		//	std::cout << "menor valor aplpha"

		//			<< alpha_solid << std::endl;

		//	std::cout << "melhor valor alpha para conectar um componente "

		//			<< *opt << std::endl;

		as.set_alpha(*opt);
	} else {
		as.set_alpha(16);
	}
	// -------------------------------------------------------
	// off arquivo

	//CGAL::set_ascii_mode(std::cout);

	std::ofstream of(fileoutput.c_str());

	/// coleta as faces regulares
	std::vector<Alpha_shape_3::Facet> facets;
	as.get_alpha_shape_facets(std::back_inserter(facets),
			Alpha_shape_3::REGULAR);

	std::stringstream pts;
	std::stringstream ind;

	std::size_t nbf = facets.size();

	for (std::size_t i = 0; i < nbf; ++i) {
		//sempre considera a cell exterior
		if (as.classify(facets[i].first) != Alpha_shape_3::EXTERIOR)
			facets[i] = as.mirror_facet(facets[i]);

		// retirar a asserção
		//CGAL_assertion(as.classify(facets[i].first) == Alpha_shape_3::EXTERIOR);

		int indices[3] = { (facets[i].second + 1) % 4, (facets[i].second + 2)
				% 4, (facets[i].second + 3) % 4, };

		///necessario para  a orientação
		if (facets[i].second % 2 == 0)
			std::swap(indices[0], indices[1]);

		pts << facets[i].first->vertex(indices[0])->point() << "\n"
				<< facets[i].first->vertex(indices[1])->point() << "\n"
				<< facets[i].first->vertex(indices[2])->point() << "\n";

		ind << "3 " << 3 * i << " " << 3 * i + 1 << " " << 3 * i + 2 << "\n";
	}

	of << "OFF \n" << 3 * nbf << " " << nbf << " 0\n";
	of << pts.str();
	of << ind.str();

	//------------------------------------------------------------------------------------

	//assert(as.number_of_solid_components() == 1);

	return fileoutput;
}
