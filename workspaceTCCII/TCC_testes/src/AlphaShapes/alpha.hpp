#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Alpha_shape_3.h>
#include <fstream>
#include <list>
#include <cassert>
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Alpha_shape_vertex_base_3<K> Vb;
typedef CGAL::Alpha_shape_cell_base_3<K> Fb;
typedef CGAL::Triangulation_data_structure_3<Vb, Fb> Tds;
typedef CGAL::Delaunay_triangulation_3<K, Tds, CGAL::Fast_location> Delaunay;
typedef CGAL::Alpha_shape_3<Delaunay> Alpha_shape_3;
typedef K::Point_3 Point;
typedef Alpha_shape_3::Alpha_iterator Alpha_iterator;
typedef Alpha_shape_3::NT NT;

/**
 * Executa o algoritmo AlphaShapes para a entrada
 *
 *@param inputFile Arquivo de entrada da nuvem de pontos
 *@param alldatapoints Conforme doc do cgal padrão (false)
 *@param alphaValue valor aplha -1 para encontrar o valor optimo (DEfault)
 * Doc do CGAL:
 * find the minimum alpha that satisfies the properties
 *
 * false converte->
 * (1) nb_components solid components <= nb_components
 *
 * true converte ->
 * (2) all data points on the boundary or in its interior
 *
 */
std::string alphaShapes(std::string inputFile, bool alldatapoints = false, int alphaValue=-1);
