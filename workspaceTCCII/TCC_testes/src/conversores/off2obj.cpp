/*
 * off2obj.cpp
 *
 *  Created on: 30/09/2014
 *      Author: root
 */

#include "off2obj.hpp"

std::string off2obj(std::string inputFileOFF) {

	std::string input;
	std::string fileoutput = (inputFileOFF + ".obj");

	std::ifstream is(inputFileOFF.c_str());
	std::ofstream os(fileoutput.c_str());

	if (!(is >> input) || input != "OFF")
		throw "Erro de conversão";
	int numvertices;
	int numfaces;
	int numedges;
	if (!(is >> numvertices >> numfaces >> numedges) || numedges > 0)
		throw "Erro de conversão";
	std::vector<double> vertices(numvertices * 3);
	std::vector<std::vector<int> > faces(numfaces);
	for (int i = 0; i < numvertices; i++) {
		os << "v";
		for (int v = 0; v < 3; v++) {
			double coord;
			is >> coord;
			os << ' ' << coord;
		}
		os << std::endl;
	}
	for (int i = 0; i < numfaces; i++) {
		int vcount;
		if (!(is >> vcount) || vcount < 3)
			throw "Erro de conversão";
		os << "f ";
		for (int v = 0; v < vcount; v++) {
			int index;
			is >> index;
			os << ' ' << index + 1;
		}
		os << std::endl;
	}

	is.close();
	os.close();
	return fileoutput;
}

