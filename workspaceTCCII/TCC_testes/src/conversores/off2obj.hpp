/*
 * off2obj.hpp
 *
 *  Created on: 30/10/2014
 *      Author: root
 */

#ifndef OFF2OBJ_HPP_
#define OFF2OBJ_HPP_

#include <fstream>
#include <string>
#include <vector>

std::string off2obj(std::string inputFileOFF);

#endif /* OFF2OBJ_HPP_ */
