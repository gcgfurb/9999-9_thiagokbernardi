#include "skin.hpp"

// shirinkm factor o valor default é 0.01
std::string Skin(std::string file, double shrinkfactor) {

	std::string fileoutput = (file + ".off");

	std::list<Weighted_point> l;

	std::ifstream is(file.c_str());

	int n;

	Bare_point p;

	for (; is >> p; n++) {
		l.push_front(Weighted_point(p));
	}

	Polyhedron P;

	CGAL::make_skin_surface_mesh_3(P, l.begin(), l.end(), shrinkfactor);

	std::ofstream of(fileoutput.c_str());

	// Escreve o poliedro no formato .OFF

	// força o  ascII
	//CGAL::set_ascii_mode(of);

	// escreve o cabecalho
	of << "OFF" << std::endl;
	// num de vertice , numero de faces sem edges
	of << P.size_of_vertices() << ' ' << P.size_of_facets() << " 0"
			<< std::endl;
	// Copia os pontos para o arquivo
	std::copy(P.points_begin(), P.points_end(),
			std::ostream_iterator<Point_3>(of, "\n"));

	// gera as faces para o arquivo
	for (Facet_iterator i = P.facets_begin(); i != P.facets_end(); ++i) {
		Halfedge_facet_circulator j = i->facet_begin();
		// AS faces do poliedro são pelo menos triangulos. necessario faze4r a asserção
		CGAL_assertion(CGAL::circulator_size(j) >= 3);
		of << CGAL::circulator_size(j) << ' ';
		do {
			of << ' ' << std::distance(P.vertices_begin(), j->vertex());
		} while (++j != i->facet_begin());
		of << std::endl;
	}

	return fileoutput;
}
