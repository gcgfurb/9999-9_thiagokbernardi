var math_8cpp =
[
    [ "crossabc", "math_8cpp.html#a3ab32ba2a8b045c1c2c4bb765f54d081", null ],
    [ "dir_and_dist", "math_8cpp.html#a5f1fe50b773126ebd5564723ee8a193a", null ],
    [ "dotabac", "math_8cpp.html#a5da1a32b3016eb01d81515239693c8e8", null ],
    [ "dotabc", "math_8cpp.html#ae72da21aef96f31f9ed3a2e82e878c7b", null ],
    [ "normalize", "math_8cpp.html#aa9e462dd8b4a18506a5b715304fcec4b", null ],
    [ "sqdist", "math_8cpp.html#a9da638309fda6e3f2584fba28c88112b", null ],
    [ "tetcircumcenter", "math_8cpp.html#af1c6e9c170e2c100154927faab642e80", null ],
    [ "tetorthocenter", "math_8cpp.html#a0523f05d3d24a2608b94469a09690d28", null ],
    [ "tricircumcenter3d", "math_8cpp.html#a6ee6d2395f6fb446d19bae60a6da0ae5", null ],
    [ "triorthocenter", "math_8cpp.html#af26d0a1791d1170a200cab0a57e6a430", null ]
];