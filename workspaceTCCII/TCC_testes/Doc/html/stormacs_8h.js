var stormacs_8h =
[
    [ "copy_simp", "stormacs_8h.html#a04afac7684ee1b4bbede41d7bd15348e", null ],
    [ "dec_ref", "stormacs_8h.html#a87aac4fb0e5d23e829a7cd06b6f62412", null ],
    [ "free_simp", "stormacs_8h.html#a7ced179f655ac37865108b28a59275a0", null ],
    [ "FREEL", "stormacs_8h.html#a5a474789c5787900e027be82a9ea8547", null ],
    [ "inc_ref", "stormacs_8h.html#a7eb647e3dfaa404216360b45b2885d31", null ],
    [ "INCP", "stormacs_8h.html#aad602e21cc866e8b4f91957fc1421def", null ],
    [ "max_blocks", "stormacs_8h.html#a6cfbc328b10251f147ea979996e5cf65", null ],
    [ "mod_refs", "stormacs_8h.html#ac2784db8ea753e842810664b8f1a9963", null ],
    [ "NEWL", "stormacs_8h.html#a4f816f3a889de2378735c047c329aad0", null ],
    [ "NEWLRC", "stormacs_8h.html#af4e2d4e07ee5eaf4d3ab7f83f33270e6", null ],
    [ "Nobj", "stormacs_8h.html#aed1b4d33270d72c0d0feeda035a11d82", null ],
    [ "NULLIFY", "stormacs_8h.html#a6cfed198c37f9b9a30855f67665416f4", null ],
    [ "STORAGE", "stormacs_8h.html#a03ea6ec7a9755c0faf96a18864942232", null ],
    [ "STORAGE_GLOBALS", "stormacs_8h.html#ae154832b2ae806308679d3499ebeb806", null ]
];