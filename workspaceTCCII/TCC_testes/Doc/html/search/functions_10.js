var searchData=
[
  ['test_5ftemp',['test_temp',['../hull_8h.html#a1833e6a9649469e6ef2611e6007194c1',1,'hull.h']]],
  ['tetcircumcenter',['tetcircumcenter',['../hull_8h.html#aa0adde63a7fac7b77bed65a83aff433f',1,'tetcircumcenter(double *, double *, double *, double *, double *, double *):&#160;math.cpp'],['../math_8cpp.html#af1c6e9c170e2c100154927faab642e80',1,'tetcircumcenter(double *a, double *b, double *c, double *d, double *circumcenter, double *cond):&#160;math.cpp']]],
  ['tetorthocenter',['tetorthocenter',['../math_8cpp.html#a0523f05d3d24a2608b94469a09690d28',1,'tetorthocenter(double a[], double b[], double c[], double d[], double orthocenter[], double *cnum):&#160;math.cpp'],['../power_8cpp.html#a0523f05d3d24a2608b94469a09690d28',1,'tetorthocenter(double a[], double b[], double c[], double d[], double orthocenter[], double *cnum):&#160;math.cpp']]],
  ['thinaxis',['thinaxis',['../hull_8h.html#a9635e7db84e2333fb3fb5cc374066223',1,'hull.h']]],
  ['to_5flower_5fcopy',['to_lower_copy',['../namespacehttpserver_1_1string__utilities.html#a37ede578e8855afc959aa341b4fe01ff',1,'httpserver::string_utilities']]],
  ['to_5fupper',['to_upper',['../namespacehttpserver_1_1string__utilities.html#ab637349113055339f9b0162243e394dc',1,'httpserver::string_utilities']]],
  ['to_5fupper_5fcopy',['to_upper_copy',['../namespacehttpserver_1_1string__utilities.html#a8dc52ba52ce91d7a5aa5e994440ceb73',1,'httpserver::string_utilities']]],
  ['tokenize_5furl',['tokenize_url',['../classhttpserver_1_1http_1_1http__utils.html#aeb3957a9e53ae78b02d453539b222e5c',1,'httpserver::http::http_utils']]],
  ['triangulacao',['Triangulacao',['../triangulacao__basica_8cpp.html#a3a8fcfa4d268708991bd2669d8f8872d',1,'Triangulacao(std::string file):&#160;triangulacao_basica.cpp'],['../triangulacao__basica_8hpp.html#a3a8fcfa4d268708991bd2669d8f8872d',1,'Triangulacao(std::string file):&#160;triangulacao_basica.cpp']]],
  ['tricircumcenter3d',['tricircumcenter3d',['../hull_8h.html#a58997ad4ecbefe4e9bff434d7de61485',1,'tricircumcenter3d(double *, double *, double *, double *, double *):&#160;math.cpp'],['../math_8cpp.html#a6ee6d2395f6fb446d19bae60a6da0ae5',1,'tricircumcenter3d(double *a, double *b, double *c, double *circumcenter, double *cond):&#160;math.cpp']]],
  ['triorthocenter',['triorthocenter',['../math_8cpp.html#af26d0a1791d1170a200cab0a57e6a430',1,'triorthocenter(double a[], double b[], double c[], double orthocenter[], double *cnum):&#160;math.cpp'],['../power_8cpp.html#af26d0a1791d1170a200cab0a57e6a430',1,'triorthocenter(double a[], double b[], double c[], double orthocenter[], double *cnum):&#160;math.cpp']]],
  ['truet',['truet',['../hull_8cpp.html#abac3180860c97614b98651beeaf4bbbb',1,'hull.cpp']]]
];
