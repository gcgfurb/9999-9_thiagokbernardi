var searchData=
[
  ['neigh',['neigh',['../structsimplex.html#a70e33a7f4e99e603cb85dc5783d57ce0',1,'simplex::neigh()'],['../classneighbors.html#a6a9836a23fdd5b0760b86871fdaf1be2',1,'neighbors::neigh()']]],
  ['neighbors',['neighbors',['../classvertex.html#ae3b8b3c8e0c89700822a611ad8543a5b',1,'vertex']]],
  ['next',['next',['../structbasis__s.html#a49f1dc276e9448cb672d6f95d8a41412',1,'basis_s::next()'],['../structsimplex.html#a375f0b3e356fabc2857fa3f89c72d6f6',1,'simplex::next()'],['../structspole.html#a8902fd144589f8c96ef4284ca316baa1',1,'spole::next()'],['../structplist.html#a3c2c370858b9d71608b0ff250853c9e9',1,'plist::next()'],['../structedgesimp.html#aaa2b6be11d0ce9fa7c850511f320b32b',1,'edgesimp::next()'],['../structqueuenode.html#afd7cf6ca18404f336910348328330bf1',1,'queuenode::next()'],['../structtree__node.html#aac6fa1efde23c0732d90538c07c6b6c6',1,'tree_node::next()'],['../structfg__node.html#a76306bab45b3a017143345b4393556e7',1,'fg_node::next()']]],
  ['nodes',['nodes',['../classface.html#ae95bfb25f174e903869826f177167a42',1,'face']]],
  ['noisethreshold',['noiseThreshold',['../classpowershape.html#a2eb756d6dbc4a81a89ed9fde4dde9bbe',1,'powershape']]],
  ['normal',['normal',['../structsimplex.html#a490996f38fab1bb0a96e40e878e8c9a4',1,'simplex']]],
  ['novert',['novert',['../structtemp.html#a8ed0a85df14466a9305e4e3890985898',1,'temp']]],
  ['nt',['nt',['../powershape_8cpp.html#ad39423a7b14094ed17af35a0603f1a33',1,'powershape.cpp']]],
  ['num_5faxedgs',['num_axedgs',['../hullmain_8cpp.html#a31675dd04da40f487c7442dc576ab7b3',1,'num_axedgs():&#160;hullmain.cpp'],['../power_8cpp.html#a31675dd04da40f487c7442dc576ab7b3',1,'num_axedgs():&#160;hullmain.cpp']]],
  ['num_5faxfaces',['num_axfaces',['../hullmain_8cpp.html#a27bae1dd70b1040745b4bd73ac989cdd',1,'num_axfaces():&#160;hullmain.cpp'],['../power_8cpp.html#a27bae1dd70b1040745b4bd73ac989cdd',1,'num_axfaces():&#160;hullmain.cpp']]],
  ['num_5fblocks',['num_blocks',['../hullmain_8cpp.html#a9bfd163a083925243621097f52863412',1,'num_blocks():&#160;hullmain.cpp'],['../pointsites_8h.html#a9bfd163a083925243621097f52863412',1,'num_blocks():&#160;pointsites.h']]],
  ['num_5ffaces',['num_faces',['../hullmain_8cpp.html#a4c32bfca58180c239b4eef4287fef2c3',1,'num_faces():&#160;hullmain.cpp'],['../power_8cpp.html#a4c32bfca58180c239b4eef4287fef2c3',1,'num_faces():&#160;hullmain.cpp']]],
  ['num_5fpoles',['num_poles',['../hullmain_8cpp.html#ade401fcfac3f6bfc2350eb4eaadcaff4',1,'num_poles():&#160;hullmain.cpp'],['../power_8cpp.html#ade401fcfac3f6bfc2350eb4eaadcaff4',1,'num_poles():&#160;hullmain.cpp']]],
  ['num_5fsites',['num_sites',['../crust_8cpp.html#a3c7108bf401c14d7af54519b7895d437',1,'num_sites():&#160;hullmain.cpp'],['../hullmain_8cpp.html#a3c7108bf401c14d7af54519b7895d437',1,'num_sites():&#160;hullmain.cpp']]],
  ['num_5ftempptr',['num_tempptr',['../structtarr.html#a84f5d8caefca03762df4a354bfb63f1b',1,'tarr']]],
  ['num_5fvtxs',['num_vtxs',['../hullmain_8cpp.html#aff1a846e2158f9db187d6c36470dc53e',1,'num_vtxs():&#160;hullmain.cpp'],['../power_8cpp.html#aff1a846e2158f9db187d6c36470dc53e',1,'num_vtxs():&#160;hullmain.cpp']]],
  ['numedges',['numedges',['../classpowercrust.html#af8af9b155fed1188a9f50974de1f6704',1,'powercrust']]],
  ['numfaces',['numfaces',['../classpowercrust.html#a8a7862a8c2813c00b2f4aba9971fbd58',1,'powercrust::numfaces()'],['../power_8cpp.html#afb09eed4a496c17806e3c1d951e9caff',1,'numfaces():&#160;power.cpp']]],
  ['numvtxs',['numvtxs',['../power_8cpp.html#a90cf380efd393c13b5b19aaf5df11591',1,'power.cpp']]]
];
