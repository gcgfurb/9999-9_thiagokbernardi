var searchData=
[
  ['fast_5ftwo_5fdiff',['Fast_Two_Diff',['../predicates_8cpp.html#a2dc0302d787df9335fab8a094e3f9c8a',1,'predicates.cpp']]],
  ['fast_5ftwo_5fdiff_5ftail',['Fast_Two_Diff_Tail',['../predicates_8cpp.html#aabefa8f7315771dc0c0ca2d7f61879b1',1,'predicates.cpp']]],
  ['fast_5ftwo_5fsum',['Fast_Two_Sum',['../predicates_8cpp.html#ae85afbfee038480d9baf7796514cb121',1,'predicates.cpp']]],
  ['fast_5ftwo_5fsum_5ftail',['Fast_Two_Sum_Tail',['../predicates_8cpp.html#afdb937df435c24d5c402eb0fdd97f6da',1,'predicates.cpp']]],
  ['first',['FIRST',['../hull_8h.html#a492c6ccf2bde2bebcd32e73e0933ee92',1,'hull.h']]],
  ['first_5fedge',['FIRST_EDGE',['../hull_8h.html#a2649f03fbe183641ca11262532526bc4',1,'hull.h']]],
  ['fixed',['FIXED',['../hull_8h.html#a20207ffc8e6f0bdcefb5c42a5b5c3fb8',1,'hull.h']]],
  ['four_5ffour_5fsum',['Four_Four_Sum',['../predicates_8cpp.html#a466f1351b3b0c629ce26624e9c4f37c8',1,'predicates.cpp']]],
  ['four_5fone_5fproduct',['Four_One_Product',['../predicates_8cpp.html#a058c146b098bad5240f5602ff0af5c65',1,'predicates.cpp']]],
  ['four_5fone_5fsum',['Four_One_Sum',['../predicates_8cpp.html#a44216cd8f5e66887021d5adc5946fd5b',1,'predicates.cpp']]],
  ['four_5ftwo_5fsum',['Four_Two_Sum',['../predicates_8cpp.html#abfb2851ae0a5a306b284bc0b49c44a58',1,'predicates.cpp']]],
  ['free_5fsimp',['free_simp',['../stormacs_8h.html#a7ced179f655ac37865108b28a59275a0',1,'stormacs.h']]],
  ['freel',['FREEL',['../stormacs_8h.html#a5a474789c5787900e027be82a9ea8547',1,'stormacs.h']]]
];
