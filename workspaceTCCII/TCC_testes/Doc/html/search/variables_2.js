var searchData=
[
  ['b',['B',['../ch_8cpp.html#ac5804e95953206e3bffbc715aa0acd30',1,'ch.cpp']]],
  ['b_5ferr_5fmin',['b_err_min',['../ch_8cpp.html#a3c23b89b067b3f3d34bb19dc2ba32577',1,'ch.cpp']]],
  ['b_5ferr_5fmin_5fsq',['b_err_min_sq',['../ch_8cpp.html#a30d54f5bf07d81c60d0cd16265772f59',1,'ch.cpp']]],
  ['bad',['bad',['../structpolelabel.html#a6c6e791e9a48eb593277a0eeb030a052',1,'polelabel']]],
  ['basis',['basis',['../structneighbor.html#a4149bc41dd5e0c6b0111bd7cef557721',1,'neighbor']]],
  ['basis_5fvec_5fsize',['basis_vec_size',['../ch_8cpp.html#a1b0687668b39f7ea724c9988b943b548',1,'ch.cpp']]],
  ['bigt',['bigt',['../ch_8cpp.html#aea1fbe75673896efdd31b5be8c01112b',1,'ch.cpp']]],
  ['boundb',['boundB',['../crust_8cpp.html#a88c21675fe91c04028efae4e3297c325',1,'boundB():&#160;hull.h'],['../hull_8h.html#a4d77e7d79495dd7532edcfd04a22e4aa',1,'boundB():&#160;hull.h'],['../hullmain_8cpp.html#a4d77e7d79495dd7532edcfd04a22e4aa',1,'boundB():&#160;hullmain.cpp']]]
];
