var searchData=
[
  ['ban_5fip',['ban_ip',['../classhttpserver_1_1webserver.html#a446afcb280eac205a34f22fa75346515',1,'httpserver::webserver']]],
  ['ban_5fsystem',['ban_system',['../classhttpserver_1_1create__webserver.html#acdd5dc6411888c8934b2cae9d389d7f1',1,'httpserver::create_webserver']]],
  ['basic_5fauth',['basic_auth',['../classhttpserver_1_1create__webserver.html#aefa3f7996de7755516870c0dbec43071',1,'httpserver::create_webserver']]],
  ['basic_5fauth_5ffail_5fresponse',['basic_auth_fail_response',['../classhttpserver_1_1http__response__builder.html#ad932b146c8b768e6d6e479061661062e',1,'httpserver::http_response_builder']]],
  ['bind_5faddress',['bind_address',['../classhttpserver_1_1create__webserver.html#ab63e0978840212e4dcc9f9aac82d4858',1,'httpserver::create_webserver']]],
  ['bind_5fsocket',['bind_socket',['../classhttpserver_1_1create__webserver.html#ad81756673b752aca1f7a490f9f3becf9',1,'httpserver::create_webserver']]],
  ['binder',['binder',['../classhttpserver_1_1details_1_1binders_1_1binder.html#a4d56fb3995cd7b01e8ae10c209bfb138',1,'httpserver::details::binders::binder::binder()'],['../classhttpserver_1_1details_1_1binders_1_1binder.html#a34d8467ad1d346cb757fdc3df0096da0',1,'httpserver::details::binders::binder::binder(const binder &amp;o)'],['../classhttpserver_1_1details_1_1binders_1_1binder.html#ab090f56521556fee0fa0f5ae50bc7492',1,'httpserver::details::binders::binder::binder(X *pmem, Y fun)'],['../classhttpserver_1_1details_1_1binders_1_1binder.html#a4c746e0c97acecde7ca41e387f1c800e',1,'httpserver::details::binders::binder::binder(DC *pp, parent_invoker invoker, static_function fun)']]],
  ['build_5fconvex_5fhull',['build_convex_hull',['../ch_8cpp.html#a3fbaa9896969e5fdf5cbf2ba59d39ba8',1,'build_convex_hull(gsitef *get_s, site_n *site_numm, short dim, short vdd):&#160;ch.cpp'],['../hull_8h.html#a0fe2627dcb98c2600e5d52ee661ab914',1,'build_convex_hull(gsitef *, site_n *, short, short):&#160;ch.cpp']]],
  ['build_5ffg',['build_fg',['../fg_8cpp.html#a48b50b4a5e0c97b302e709f076de2e95',1,'build_fg(simplex *root):&#160;fg.cpp'],['../hull_8h.html#a3c09800632e7f6062acb3558def15903',1,'build_fg(simplex *):&#160;fg.cpp']]],
  ['buildhull',['buildhull',['../hull_8cpp.html#a04073048c36de82c781d6de17befa992',1,'buildhull(simplex *root):&#160;hull.cpp'],['../hull_8h.html#a101c2f7e5d5059b0949bbc9c5e8efb81',1,'buildhull(simplex *):&#160;hull.cpp']]],
  ['byte_5fstring',['byte_string',['../structhttpserver_1_1byte__string.html#af0c098e5165ce817e0a84fd0f368cca2',1,'httpserver::byte_string']]]
];
