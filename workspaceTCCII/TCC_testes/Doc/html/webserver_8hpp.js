var webserver_8hpp =
[
    [ "http_resource", "singletonhttpserver_1_1http__resource.html", "singletonhttpserver_1_1http__resource" ],
    [ "event_supplier", "classhttpserver_1_1event__supplier.html", "classhttpserver_1_1event__supplier" ],
    [ "webserver", "classhttpserver_1_1webserver.html", "classhttpserver_1_1webserver" ],
    [ "GENERIC_ERROR", "webserver_8hpp.html#a88d838087aab6c53d2977ed91c58753e", null ],
    [ "METHOD_ERROR", "webserver_8hpp.html#a36fa962a657e97c250691f73e8fff4a2", null ],
    [ "NOT_FOUND_ERROR", "webserver_8hpp.html#aed6a8ef0e164b3aff36664fc08c30b1d", null ],
    [ "NOT_METHOD_ERROR", "webserver_8hpp.html#a2840f2e0870fd0d750390ba86cd975cf", null ]
];