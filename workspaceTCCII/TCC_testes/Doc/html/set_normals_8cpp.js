var set_normals_8cpp =
[
    [ "neighbors", "classneighbors.html", "classneighbors" ],
    [ "powercrust", "classpowercrust.html", "classpowercrust" ],
    [ "CORRECT", "set_normals_8cpp.html#a2e70e63f571196dc39675a7e2b809b6f", null ],
    [ "WRONG", "set_normals_8cpp.html#a2f2c9b4b416a1754ea8a01b68798b175", null ],
    [ "mainNormals", "set_normals_8cpp.html#abf2b11a8622bc80d622260fc32efb8e1", null ],
    [ "parseCommandNormals", "set_normals_8cpp.html#a216e1e71500574bc9eeeb2deae7f3358", null ],
    [ "inFileNameNormals", "set_normals_8cpp.html#a0a1e4591907317d6a6fa646e89ff670b", null ],
    [ "outFileNameNormals", "set_normals_8cpp.html#a6106a7cbbe87b1fbd727aca4dbf72a7e", null ],
    [ "reverseFaces", "set_normals_8cpp.html#ad7910d14e9f37c07a55bb69e129ae53c", null ]
];