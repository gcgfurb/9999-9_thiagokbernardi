var create__webserver_8hpp =
[
    [ "create_webserver", "classhttpserver_1_1create__webserver.html", "classhttpserver_1_1create__webserver" ],
    [ "DEFAULT_WS_PORT", "create__webserver_8hpp.html#a157c2e6a4c36b61215b97d0ec05426c6", null ],
    [ "DEFAULT_WS_TIMEOUT", "create__webserver_8hpp.html#af3bfffd8935aa1542761341388fb660d", null ],
    [ "log_access_ptr", "create__webserver_8hpp.html#a7d4eaae1fdf3d06baee29220fc51636d", null ],
    [ "log_error_ptr", "create__webserver_8hpp.html#afd84ce7c38228d4846ed24fee943f272", null ],
    [ "render_ptr", "create__webserver_8hpp.html#a74a5cebb29b29de869b58561df4d86e0", null ],
    [ "unescaper_ptr", "create__webserver_8hpp.html#a7495f7c63add489bdc55b7645c31bed0", null ],
    [ "validator_ptr", "create__webserver_8hpp.html#a108830e5112295a9edc5ad91c45408e1", null ]
];