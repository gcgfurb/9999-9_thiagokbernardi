var classpowershape =
[
    [ "powershape", "classpowershape.html#a186d33c3e62e80347813c795b93ea9a5", null ],
    [ "powershape", "classpowershape.html#a439f78a29e09d520cbd35de9baa55c07", null ],
    [ "noiseRemove", "classpowershape.html#ab9b2ab7db2c0f87472944bdea9fa0a6e", null ],
    [ "outputPlot", "classpowershape.html#aaa7e471f902f483bbf5cb07b40025770", null ],
    [ "outputPoles", "classpowershape.html#a51a612a47d26c704676e970e7b6059a7", null ],
    [ "readInput", "classpowershape.html#ad66895357572abc48b17b216a1e53b51", null ],
    [ "redRemove", "classpowershape.html#a0d63eafcdc97edbf0b8a74e8433ba3bc", null ],
    [ "noiseThreshold", "classpowershape.html#a2eb756d6dbc4a81a89ed9fde4dde9bbe", null ],
    [ "redThreshold", "classpowershape.html#a1fc5582d422d7d30619c31391748aede", null ],
    [ "verts", "classpowershape.html#a56d99d6a7854e4bf529607d67d8b1774", null ]
];