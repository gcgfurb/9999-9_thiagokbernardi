var dir_48720184cab8cd15d350f41b34a07f8d =
[
    [ "ch.cpp", "ch_8cpp.html", "ch_8cpp" ],
    [ "crust.cpp", "crust_8cpp.html", "crust_8cpp" ],
    [ "fg.cpp", "fg_8cpp.html", "fg_8cpp" ],
    [ "getopt.cpp", "getopt_8cpp.html", "getopt_8cpp" ],
    [ "getopt.h", "getopt_8h.html", "getopt_8h" ],
    [ "heap.cpp", "heap_8cpp.html", "heap_8cpp" ],
    [ "hull.cpp", "hull_8cpp.html", "hull_8cpp" ],
    [ "hull.h", "hull_8h.html", "hull_8h" ],
    [ "hullmain.cpp", "hullmain_8cpp.html", "hullmain_8cpp" ],
    [ "hullmain.hpp", "hullmain_8hpp.html", "hullmain_8hpp" ],
    [ "io.cpp", "io_8cpp.html", "io_8cpp" ],
    [ "label.cpp", "label_8cpp.html", "label_8cpp" ],
    [ "math.cpp", "math_8cpp.html", "math_8cpp" ],
    [ "ndefs.h", "ndefs_8h.html", [
      [ "face", "classface.html", "classface" ],
      [ "edge", "classedge.html", "classedge" ],
      [ "vertex", "classvertex.html", "classvertex" ]
    ] ],
    [ "pointops.cpp", "pointops_8cpp.html", "pointops_8cpp" ],
    [ "points.h", "points_8h.html", "points_8h" ],
    [ "pointsites.h", "pointsites_8h.html", "pointsites_8h" ],
    [ "power.cpp", "power_8cpp.html", "power_8cpp" ],
    [ "powershape.cpp", "powershape_8cpp.html", "powershape_8cpp" ],
    [ "predicates.cpp", "predicates_8cpp.html", "predicates_8cpp" ],
    [ "rand.cpp", "rand_8cpp.html", "rand_8cpp" ],
    [ "randStuff.h", "rand_stuff_8h.html", "rand_stuff_8h" ],
    [ "sdefs.h", "sdefs_8h.html", "sdefs_8h" ],
    [ "setNormals.cpp", "set_normals_8cpp.html", "set_normals_8cpp" ],
    [ "stormacs.h", "stormacs_8h.html", "stormacs_8h" ]
];