var classvertex =
[
    [ "vertex", "classvertex.html#a255e93cf00946bb3f3fbf4c2d2a4f126", null ],
    [ "vertex", "classvertex.html#a3c9ca83f82d88e0e1249e0afbdbd5002", null ],
    [ "distance", "classvertex.html#a090b2a702d295e305e0bea6630fb56f8", null ],
    [ "index", "classvertex.html#a0a48fa407a7556d877513bba7d4048a7", null ],
    [ "label", "classvertex.html#ad96d25f4031373cb7033042443b6d949", null ],
    [ "neighbors", "classvertex.html#ae3b8b3c8e0c89700822a611ad8543a5b", null ],
    [ "radius", "classvertex.html#ab27834a7ede7838d766ecc7e509a1609", null ],
    [ "status", "classvertex.html#af3e86c3ff682cdc5a5fff406f9efff12", null ],
    [ "x", "classvertex.html#a11f52ec2e920d56500baefe5a2e2bba7", null ],
    [ "y", "classvertex.html#a8b9f211498390a67c369fd43f3722a19", null ],
    [ "z", "classvertex.html#a4e4f9696a91e9bee75a4c15d8d77e064", null ]
];