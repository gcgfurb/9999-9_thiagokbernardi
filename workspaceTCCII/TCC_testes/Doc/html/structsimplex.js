var structsimplex =
[
    [ "edgestatus", "structsimplex.html#a4ffcccdfa33b152a17fb58fb9733825a", null ],
    [ "mark", "structsimplex.html#ab86f8e1d6871c48321652fc938c22b9d", null ],
    [ "neigh", "structsimplex.html#a70e33a7f4e99e603cb85dc5783d57ce0", null ],
    [ "next", "structsimplex.html#a375f0b3e356fabc2857fa3f89c72d6f6", null ],
    [ "normal", "structsimplex.html#a490996f38fab1bb0a96e40e878e8c9a4", null ],
    [ "peak", "structsimplex.html#a08e83e9125cca8eab4e9f60526440564", null ],
    [ "poleindex", "structsimplex.html#ab8401ea3573979152f8b706294a295a6", null ],
    [ "sqradius", "structsimplex.html#ae8b6d5f09d760fb5bbabaf5fd554d0e9", null ],
    [ "status", "structsimplex.html#a717d0c4a381279377ff0214135408022", null ],
    [ "visit", "structsimplex.html#aa66100a5d9fc4a9024f9e74bdee1c88b", null ],
    [ "vv", "structsimplex.html#a05ea99e5185e456328469bb9598f95b0", null ]
];