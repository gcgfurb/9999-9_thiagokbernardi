var classhttpserver_1_1http_1_1http__utils =
[
    [ "cred_type_T", "classhttpserver_1_1http_1_1http__utils.html#aa0a27d0504ea189b62531879d922ec04", [
      [ "NONE", "classhttpserver_1_1http_1_1http__utils.html#aa0a27d0504ea189b62531879d922ec04a8412ec3893f052e34502e34ae33ac442", null ]
    ] ],
    [ "IP_version_T", "classhttpserver_1_1http_1_1http__utils.html#a25967744f904719723a7782c83b6ca7b", [
      [ "IPV4", "classhttpserver_1_1http_1_1http__utils.html#a25967744f904719723a7782c83b6ca7ba1318d22f917de0381d5863f6dbc42def", null ],
      [ "IPV6", "classhttpserver_1_1http_1_1http__utils.html#a25967744f904719723a7782c83b6ca7ba97d08aa9b47340a186c502e1c8effcc8", null ]
    ] ],
    [ "policy_T", "classhttpserver_1_1http_1_1http__utils.html#a183a09e229c47fb4f5e8d3679927f9f2", [
      [ "ACCEPT", "classhttpserver_1_1http_1_1http__utils.html#a183a09e229c47fb4f5e8d3679927f9f2a60e12151cef9cda31d486834ca61c5a7", null ],
      [ "REJECT", "classhttpserver_1_1http_1_1http__utils.html#a183a09e229c47fb4f5e8d3679927f9f2a8fb24206f3cfb616e1e1b5e48ee9f4fc", null ]
    ] ],
    [ "start_method_T", "classhttpserver_1_1http_1_1http__utils.html#a3e77359e2d0ccc85d05ad6c93a31c2de", [
      [ "INTERNAL_SELECT", "classhttpserver_1_1http_1_1http__utils.html#a3e77359e2d0ccc85d05ad6c93a31c2dea0f645ddba22a3581fdda4aa6dcdca649", null ],
      [ "THREADS", "classhttpserver_1_1http_1_1http__utils.html#a3e77359e2d0ccc85d05ad6c93a31c2dea452c6a74e1b34ece08daecbb557e698a", null ],
      [ "POLL", "classhttpserver_1_1http_1_1http__utils.html#a3e77359e2d0ccc85d05ad6c93a31c2dea28d9e59d00efb64f473add76ac0b8866", null ]
    ] ]
];