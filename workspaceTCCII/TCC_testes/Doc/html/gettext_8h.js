var gettext_8h =
[
    [ "bind_textdomain_codeset", "gettext_8h.html#a973ce74418a69a456534d48dedddcd46", null ],
    [ "bindtextdomain", "gettext_8h.html#ac719c37e146c0e2030a7ed1dfcbdd5f8", null ],
    [ "dcgettext", "gettext_8h.html#a4419bece30ab76327deb6ef4149322f4", null ],
    [ "dcngettext", "gettext_8h.html#aa646ec380c28849655fcc4d56873062c", null ],
    [ "dgettext", "gettext_8h.html#a86f757e66ffde703afba316a7396a28f", null ],
    [ "dngettext", "gettext_8h.html#a06fa46950f520d32c10de24270198932", null ],
    [ "gettext", "gettext_8h.html#aa0fab2ce0e5eb897c393cee0d83c6ea9", null ],
    [ "gettext_noop", "gettext_8h.html#a1a0ef2e42374f90a13299cc06f437847", null ],
    [ "ngettext", "gettext_8h.html#a248c64613ae95f3477511c239fe9c5c1", null ],
    [ "textdomain", "gettext_8h.html#aba3653075c8f1f84c741ef23b2266f89", null ]
];