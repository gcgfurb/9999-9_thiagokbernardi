var sdefs_8h =
[
    [ "vertex", "classvertex.html", "classvertex" ],
    [ "powershape", "classpowershape.html", "classpowershape" ],
    [ "SQ", "sdefs_8h.html#a26ec7d0357099a204dd9c18d5818dcae", null ],
    [ "polelabel", "sdefs_8h.html#a4611082ef961c6a7fffe3d04b9cfbd3f", [
      [ "UNLABELED", "sdefs_8h.html#a4611082ef961c6a7fffe3d04b9cfbd3fabdcc1805fc659d979c4bedb0f6ba7c2d", null ],
      [ "EXT", "sdefs_8h.html#a4611082ef961c6a7fffe3d04b9cfbd3fa43943b97a80411494fa56399e53f17c2", null ],
      [ "INT", "sdefs_8h.html#a4611082ef961c6a7fffe3d04b9cfbd3fafd5a5f51ce25953f3db2c7e93eb7864a", null ]
    ] ],
    [ "polestatus", "sdefs_8h.html#a9924646cf1530df16c9ee18f4dd602fe", [
      [ "FIXED", "sdefs_8h.html#a9924646cf1530df16c9ee18f4dd602fea9b5eccb7f8f027c46f2018d07c74f384", null ],
      [ "PRESENT", "sdefs_8h.html#a9924646cf1530df16c9ee18f4dd602feac2ecbd8ac68f5af3d40003641f599bd8", null ],
      [ "REMOVED", "sdefs_8h.html#a9924646cf1530df16c9ee18f4dd602fea4e9ac82e8594184cb2fb8a4ef86df12d", null ]
    ] ]
];