################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/http_request.cpp \
../src/http_resource.cpp \
../src/http_response.cpp \
../src/http_utils.cpp \
../src/main_test_stub.cpp \
../src/string_utilities.cpp \
../src/webserver.cpp 

OBJS += \
./src/http_request.o \
./src/http_resource.o \
./src/http_response.o \
./src/http_utils.o \
./src/main_test_stub.o \
./src/string_utilities.o \
./src/webserver.o 

CPP_DEPS += \
./src/http_request.d \
./src/http_resource.d \
./src/http_response.d \
./src/http_utils.d \
./src/main_test_stub.d \
./src/string_utilities.d \
./src/webserver.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


