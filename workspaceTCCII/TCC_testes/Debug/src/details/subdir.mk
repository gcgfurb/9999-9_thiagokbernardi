################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/details/comet_manager.cpp \
../src/details/http_endpoint.cpp 

OBJS += \
./src/details/comet_manager.o \
./src/details/http_endpoint.o 

CPP_DEPS += \
./src/details/comet_manager.d \
./src/details/http_endpoint.d 


# Each subdirectory must supply rules for building sources it contributes
src/details/%.o: ../src/details/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include/eigen3 -O0 -g3 -Wall -c -fmessage-length=0 -frounding-math -fpermissive -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


