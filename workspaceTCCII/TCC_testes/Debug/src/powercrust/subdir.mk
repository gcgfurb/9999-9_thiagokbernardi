################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/powercrust/ch.cpp \
../src/powercrust/crust.cpp \
../src/powercrust/fg.cpp \
../src/powercrust/getopt.cpp \
../src/powercrust/heap.cpp \
../src/powercrust/hull.cpp \
../src/powercrust/hullmain.cpp \
../src/powercrust/io.cpp \
../src/powercrust/label.cpp \
../src/powercrust/math.cpp \
../src/powercrust/pointops.cpp \
../src/powercrust/power.cpp \
../src/powercrust/powershape.cpp \
../src/powercrust/predicates.cpp \
../src/powercrust/rand.cpp \
../src/powercrust/setNormals.cpp 

OBJS += \
./src/powercrust/ch.o \
./src/powercrust/crust.o \
./src/powercrust/fg.o \
./src/powercrust/getopt.o \
./src/powercrust/heap.o \
./src/powercrust/hull.o \
./src/powercrust/hullmain.o \
./src/powercrust/io.o \
./src/powercrust/label.o \
./src/powercrust/math.o \
./src/powercrust/pointops.o \
./src/powercrust/power.o \
./src/powercrust/powershape.o \
./src/powercrust/predicates.o \
./src/powercrust/rand.o \
./src/powercrust/setNormals.o 

CPP_DEPS += \
./src/powercrust/ch.d \
./src/powercrust/crust.d \
./src/powercrust/fg.d \
./src/powercrust/getopt.d \
./src/powercrust/heap.d \
./src/powercrust/hull.d \
./src/powercrust/hullmain.d \
./src/powercrust/io.d \
./src/powercrust/label.d \
./src/powercrust/math.d \
./src/powercrust/pointops.d \
./src/powercrust/power.d \
./src/powercrust/powershape.d \
./src/powercrust/predicates.d \
./src/powercrust/rand.d \
./src/powercrust/setNormals.d 


# Each subdirectory must supply rules for building sources it contributes
src/powercrust/%.o: ../src/powercrust/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include/eigen3 -O0 -g3 -Wall -c -fmessage-length=0 -frounding-math -fpermissive -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


