################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../powercrust-read-only/ch.cpp \
../powercrust-read-only/crust.cpp \
../powercrust-read-only/fg.cpp \
../powercrust-read-only/getopt.cpp \
../powercrust-read-only/heap.cpp \
../powercrust-read-only/hull.cpp \
../powercrust-read-only/hullmain.cpp \
../powercrust-read-only/io.cpp \
../powercrust-read-only/label.cpp \
../powercrust-read-only/math.cpp \
../powercrust-read-only/pointops.cpp \
../powercrust-read-only/power.cpp \
../powercrust-read-only/powershape.cpp \
../powercrust-read-only/predicates.cpp \
../powercrust-read-only/rand.cpp \
../powercrust-read-only/setNormals.cpp 

OBJS += \
./powercrust-read-only/ch.o \
./powercrust-read-only/crust.o \
./powercrust-read-only/fg.o \
./powercrust-read-only/getopt.o \
./powercrust-read-only/heap.o \
./powercrust-read-only/hull.o \
./powercrust-read-only/hullmain.o \
./powercrust-read-only/io.o \
./powercrust-read-only/label.o \
./powercrust-read-only/math.o \
./powercrust-read-only/pointops.o \
./powercrust-read-only/power.o \
./powercrust-read-only/powershape.o \
./powercrust-read-only/predicates.o \
./powercrust-read-only/rand.o \
./powercrust-read-only/setNormals.o 

CPP_DEPS += \
./powercrust-read-only/ch.d \
./powercrust-read-only/crust.d \
./powercrust-read-only/fg.d \
./powercrust-read-only/getopt.d \
./powercrust-read-only/heap.d \
./powercrust-read-only/hull.d \
./powercrust-read-only/hullmain.d \
./powercrust-read-only/io.d \
./powercrust-read-only/label.d \
./powercrust-read-only/math.d \
./powercrust-read-only/pointops.d \
./powercrust-read-only/power.d \
./powercrust-read-only/powershape.d \
./powercrust-read-only/predicates.d \
./powercrust-read-only/rand.d \
./powercrust-read-only/setNormals.d 


# Each subdirectory must supply rules for building sources it contributes
powercrust-read-only/%.o: ../powercrust-read-only/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -fpermissive -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


