This file contains information about FeatureRecon developed by 
The Jyamiti Group headed by Prof. Tamal K. Dey at
The Department of Computer Science and Engineering,
The Ohio State University.

FeatureRecon is distributed as executable code for these platforms:
1.) Windows 7/vista, XP, 2000, 98
2.) Linux (IA32), kernel 2.6

For Linux version, ANN, OpenGL and GLUT may be needed at runtime.

###########################################################################

NAME
	FeatureRecon - Extracts feature curves of a singular surface which could be 
		composed by a collection of piecewise smooth surface patches. 
		The input is a point cloud sampled on the singular surface.
		The output is extracted feature curves in polyline format. 
		FeatureRecon also samples feature curves as weighted points 
		for feature preserving purpose. This software is based on the 
		algorithm presented in paper [1].

SYNOPSIS
	Usage: ./FeatureRecon [<options>] <infile> <outfile>
	where [options] can be any of: 
		-t	  <value>		Distance ratio used for feature points filtering. (default 5.0).
		-fl	  <value>		Radius used by FeaturePCA and Filter. (default 0.1).
		-cl	  <value>		Radius used by FeatureTest to locally build weighted Voronoi diagram. (default 0.1).
		-rho1 <value>		Upper threshold of FeatureTest. The greater rho1 is, the more points close to sharp edges will be recognized. (default 0.4).
		-rho3 <value>		Lower threshold of FeatureTest. The smaller rho3 is, the more points close to non-manifold areas will be recognized. (default 0.3).
		-cos  <value>		Cosine value of the threshold angle used by NNCrust to reconstruct feature curves. (default -0.8).
		-w	  <value>		Square root of weight of samplings of feature curves. (default is twice of the average distance between a points and its nearest neighbor).
		-dc	  <value>		dc * cl determines square root of weight of the point tested by FeatureTest. (default 0.5).
		-rc	  <value>		w * rc determines square root of weight of feature points at curve junctions. (default 2.0).
		-noise				If this flag is set, pole vectors will be dumped for denoising purpose. (default false).

DESCRIPTION
	FeatureRecon extracts feature curves of a singular surface from its 
		sampled point cloud. Here, a singular surface is consisted of a 
		collection of smooth surface patches with boundaries. These surface 
		patches can intersect, or be "glued" along their common boundaries 
		(i.e, sharp crease lines). Those patches meet at non-smooth or 
		even non-manifold regions. For simplicity, we refer the region 
		where patches meet as feature curves. FeatureRecons takes an OFF 
		format file containing the sampled point cloud as the input file 
		and writes the extracted feature curves to designated OFF file. 
		Basically, FeatureRecon has two steps: feature points detection 
		and feature curves reconstruction.
		
	Feature points detection aims at find points close to feature curves. This stage 
		also marks corner points, a special kind of feature points, which are 
		located in the intersection of feature curves. FeatureRecon relies
		on weighted Delaunay triangulation and Principal Component Analysis (PCA) to 
		detect feature points. We observe that points away from feature curves
		have slim pencil shape Voronoi cells. Otherwise, they are considered
		as feature points. We use the technique proposed in [3] to analyze shapes
		of Voronoi cells. To distinguish corner points, FeatureRecon applies 
		PCA on every feature point and its neighborhood. If the results have
		two or more relative large eigenvalues, that feature point is 
		considered a corner point.
		
	FeatureRecon reconstructs feature curves in the second stage. Usually, 
		recognized feature points are distributed around possible feature 
		curves. FeatureRecon takes different ways to filter corner points
		and feature points. FeatureRecon computes the distance between
		each feature point and the barycenter of its neighbooring feature 
		points and removes feature points with large distance. For a single
		curve intersection, there could be multiple corner points. To guarantee
		the topology of feature curves, corner points are grouped. Barycenter 
		of each group replaces all the corner points in the group. Then 
		FeatureRecon applies modifed NNCrust [3] to rebuild feature curves.
		User could interactively modified reconstructed feature curves by
		moving, inserting and deleting feature points. At the end of this stage, 
		reconstructed feature cruves will be resampled to generate a evenly 
		distributed feature points.
	
EXAMPLE
	To extract feature curves of the example model, try
	FeatureRecon.exe -t 25 -fl 0.02 -dc 0 -cl 0.036 -rho3 0.32 -rho1 0.40 -rc 3 sphere_cube_pois_65K.off sphere_cube.off
	
RUNTIME OUTPUT
	STDOUT is used to prompt the progress of the execution of FeatureRecon
		whereas STDERR is used to display error messages.
	.line.off contains reconstructed feature curves in polyline format
	.sample.off contains sampling of feature curves. This file can be used with
		the origin input as the input of WeighCocone to reconstruct the singular
		surface.
	.norm contains vertices and their estimated normals for denoising purpose. 
		This file is generated only when -noise flag is set in the parameter. 
		First half of this file stores coordinate of input point cloud. Second 
		half of this file stores normals, in the same order of dumped points.

VISUALIZATION AND USER INTERACTION
	FeatureRecon has a simple visualization function. It could display 
	input point cloud, detected feature points and reconstructed feature 
	curves.
	
	Some control keys are:
	
	Page Up/Page Down - Previous/Next step(switch between feature points 
		detection and feature curves reconstruction)
	'i' - show/hide input point cloud
	'n' - show/hide axis indicator
	
	In step 1,
		Up/Down - change value of rho1
		Left/Right - change value of rho3
	
	'q' - quit FeatureRecon

LEGAL TERMS
	THIS SOFTWARE IS PROVIDED "AS-IS". THERE IS NO WARRANTY OF ANY KIND.
	NEITHER THE AUTHORS NOR THE OHIO STATE UNIVERSITY WILL BE LIABLE
	FOR ANY DAMAGES OF ANY KIND, EVEN IF ADVISED OF SUCH POSSIBILITY.
	
	This software is copyrighted by the Jyamiti research group at
	The Ohio State University.
	Please do not redistribute this software.
	This program is for academic research use only.
	
	The Windows version of this program uses CGAL 3.7.
	The Linux version of this program uses CGAL 3.6.

	The CGAL 3.7 library's license (which applies to the CGAL 
	library ONLY and NOT to this program itself) is as follows:
=============================================================================== 
LICENSE
----------------------------------------------------------------------------

The CGAL software consists of several parts, each of which is licensed under
an open source license. It is also possible to obtain commercial licenses
from GeometryFactory (www.geometryfactory.com) for all or parts of CGAL.

The source code of the CGAL library can be found in the directories
"src/CGAL", "src/CGALQt", "src/CGALQt4" and "include/CGAL" (with the
exception of "include/CGAL/CORE", "include/CGAL/OpenNL").
It is specified in each file of the CGAL library which
license applies to it. This is either the GNU Lesser General Public License
(as published by the Free Software Foundation; version 2.1 of the License)
or the Q Public License (version 1.0), *depending on each file*. The texts
of both licenses can be found in the files LICENSE.LGPL and LICENSE.QPL.  

Distributed along with CGAL (for the users' convenience), but not part of
CGAL, are the following third-party libraries, available under their own
licenses:

- CORE, in the directories "include/CGAL/CORE" and "src/CGALCore", is
  licensed under the QPL (see LICENSE.QPL).
- ImageIO, in the directory "src/CGALimageIO", is licensed under the LGPL
  (see LICENSE.LGPL).
- OpenNL, in the directory "include/CGAL/OpenNL", is licensed under the LGPL
  (see LICENSE.LGPL).

All other files that do not have an explicit copyright notice (e.g., all
examples and some demos) are licensed under a very permissive license. The
exact license text can be found in the file LICENSE.FREE_USE.

More information on the CGAL license can be found at
http://www.cgal.org/license.html

=============================================================================== 


REFERENCES
	[1] Voronoi-based Feature Curves Extraction for Sampled Singular Surfaces. 
		T. K. Dey and L. Wang, to appear. 
	[2] A Simple Provable Algorithm for Curve Reconstruction. T. K. Dey and P. Kumar. 
		Proc. 10th. ACM-SIAM Symposium on Discrete Algorithms (SODA '99) 
		1999, 893--894.
	[3] Shape Dimension and Approximation from Samples. T. K. Dey, J. Giesen,
		S. Goswami and W. Zhao. Discrete & Comput. Geom., vol 29, 419--434 (2003) 

###########################################################################
last modified: 05-09-13
1.2
###########################################################################



