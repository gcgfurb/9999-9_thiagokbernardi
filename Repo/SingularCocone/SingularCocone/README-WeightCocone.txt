This file contains information about WeightCocone developed by 
The Jyamiti Group headed by Prof. Tamal K. Dey at
The Department of Computer Science and Engineering,
The Ohio State University.

WeightCocone is distributed as executable code for these platforms:
1.) Windows 7/vista, XP, 2000, 98
2.) Linux (IA32), kernel 2.6

For Linux version, ANN may be needed at runtime.

###########################################################################

NAME
	WeightCocone - Reconstruct a singular surface which could be 
		composed by a collection of piecewise smooth surface patches. 
		The input is a point cloud sampled on the singular surface 
		and sampling of feature curves with weight. The weighted 
		sampling of feature curves can be obtained by running 
		FeatureRecon or other compatible feature extraction algorithms
		on the input point cloud.
		
		The output is a triangular mesh approximating the surface. 
		This software is based on the reconstruction algorithm presented
		in paper [1].

SYNOPSIS
	Usage: ./WeightCocone [<options>] <point cloud file> <feature sampling file>
	where [options] can be any of: 
		-c <angle>   half angle of the cocone (default pi/8).
		-r <value>   height / width ratio of cocone (default 1.2).

DESCRIPTION
	WeightCocone reconstructs a singular surface from it sampled point cloud. 
		Here, a singular surface is consisted of a collection of smooth surface
		patches with boundaries. These surface patches can intersect, or 
		be "glued" along their common boundaries (i.e, sharp crease lines). 
		Those patches meet at non-smooth or even non-manifold regions. For 
		simplicity, we refer the region where patches meet as feature curves.
		WeightCocone takes two OFF format files as input: one contains the 
		point cloud, another file with weighted sampling of feature curves.
		Weight of the sampling will be used as weight of the protecting balls.
		WeightCocone writes the generated mesh to designated OFF file.
	
	WeightCocone applies weighted Delaunay triangulation to reconstruct a
		singular surface while preserving feature curves. Each feature 
		point and corner point is assigned a protecting ball. Radii of 
		protecting balls for feature points are assigneded as twice of 
		the average distance between any sampled and its closest neighbor.
		Corner points have larger protecting balls, their radii are two 
		times of feature point protecting balls. No point from the sampled 
		point cloud has protecting ball and that point will be deleted 
		if it falls in any protecting ball. For more information, we 
		direct the reader to papers[1, 2] which explain WeightCocone 
		algorithm in details.
	
EXAMPLE
	To reconstruct the give model, try
	WeightCocone.exe sphere_cube_pois_65K.off sphere_cube.off.sample.off
	
RUNTIME OUTPUT
	STDOUT is used to prompt the progress of the execution of WeightCocone
		whereas STDERR is used to display error messages.
	.mesh.off contains the reconstructed mesh.

LEGAL TERMS
	THIS SOFTWARE IS PROVIDED "AS-IS". THERE IS NO WARRANTY OF ANY KIND.
	NEITHER THE AUTHORS NOR THE OHIO STATE UNIVERSITY WILL BE LIABLE
	FOR ANY DAMAGES OF ANY KIND, EVEN IF ADVISED OF SUCH POSSIBILITY.
	
	This software is copyrighted by the Jyamiti research group at
	The Ohio State University.
	Please do not redistribute this software.
	This program is for academic research use only.
	
	The Windows version of this program uses CGAL 3.7.
	The Linux version of this program uses CGAL 3.6.

	The CGAL 3.7 library's license (which applies to the CGAL 
	library ONLY and NOT to this program itself) is as follows:
=============================================================================== 
LICENSE
----------------------------------------------------------------------------

The CGAL software consists of several parts, each of which is licensed under
an open source license. It is also possible to obtain commercial licenses
from GeometryFactory (www.geometryfactory.com) for all or parts of CGAL.

The source code of the CGAL library can be found in the directories
"src/CGAL", "src/CGALQt", "src/CGALQt4" and "include/CGAL" (with the
exception of "include/CGAL/CORE", "include/CGAL/OpenNL").
It is specified in each file of the CGAL library which
license applies to it. This is either the GNU Lesser General Public License
(as published by the Free Software Foundation; version 2.1 of the License)
or the Q Public License (version 1.0), *depending on each file*. The texts
of both licenses can be found in the files LICENSE.LGPL and LICENSE.QPL.  

Distributed along with CGAL (for the users' convenience), but not part of
CGAL, are the following third-party libraries, available under their own
licenses:

- CORE, in the directories "include/CGAL/CORE" and "src/CGALCore", is
  licensed under the QPL (see LICENSE.QPL).
- ImageIO, in the directory "src/CGALimageIO", is licensed under the LGPL
  (see LICENSE.LGPL).
- OpenNL, in the directory "include/CGAL/OpenNL", is licensed under the LGPL
  (see LICENSE.LGPL).

All other files that do not have an explicit copyright notice (e.g., all
examples and some demos) are licensed under a very permissive license. The
exact license text can be found in the file LICENSE.FREE_USE.

More information on the CGAL license can be found at
http://www.cgal.org/license.html

=============================================================================== 


REFERENCES
	[1] Feature-Preserving Reconstruction of Singular Surfaces. T. K. Dey, 
		X. Ge, Q. Que, I. Safa, L. Wang, and Y. Wang. Computer Graphics 
		Forum, Vol. 31 (5), 1787--1796, special issue of Eurographics 
		Sympos. Geometry Processing (SGP 2012).  


###########################################################################
last modified: 05-09-13
1.2
###########################################################################



