@echo off
FeatureRecon-windows.exe -t 25 -fl 0.02 -dc 0 -cl 0.036 -rho3 0.32 -rho1 0.40 -rc 3 sphere_cube_pois_65K.off sphere_cube.off
WeightCocone-windows.exe sphere_cube_pois_65K.off sphere_cube.off.sample.off

FeatureRecon-windows.exe -t 20 -fl 0.9 -cl 2.0 -cos -0.8 -rho3 0.60 -rho1 0.35 -rc 2 better_block_50kv.off block50kv.off
WeightCocone-windows.exe better_block_50kv.off block50kv.off.sample.off
pause